//
//  UIViewController+ext.swift
//  Chapter13_IntroductionOOP
//
//  Created by Vo The Dong An on 10/17/18.
//

import UIKit

extension UIViewController {
    func showActivity(items: [Any], applicationActivities: [UIActivity]?, completion: (() -> Void)?) {
        let activityVC = UIActivityViewController(activityItems: items, applicationActivities: applicationActivities)
        present(activityVC, animated: true, completion: completion)
    }
}
