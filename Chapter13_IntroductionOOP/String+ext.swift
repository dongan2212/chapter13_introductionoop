//
//  ViewController.swift
//  Chapter13_IntroductionOOP
//
//  Created by Vo The Dong An on 10/16/18.
//
extension String {
    var trimmedAndLowerCasedString: String {
        return trimmingCharacters(in: .whitespacesAndNewlines).lowercased().replacingOccurrences(of: " ", with: "")
    }
}


