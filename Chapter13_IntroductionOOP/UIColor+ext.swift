//
//  UIColor+ext.swift
//  Chapter13_IntroductionOOP
//
//  Created by Vo The Dong An on 10/17/18.
//

import UIKit
extension UIColor {
    
    convenience init (red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        let redRGB = CGFloat(red)/255.0
        let greenRGB = CGFloat(green)/255.0
        let blueRGB = CGFloat(blue)/255.0
        self.init(red: redRGB, green: greenRGB, blue: blueRGB, alpha: 1.0)
    }
    
    static var shareButton = UIColor(red: 48, green: 173, blue: 99)
    static var deleteButton = UIColor(red: 202, green: 202, blue: 203)
}
