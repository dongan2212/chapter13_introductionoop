//
//  RestaurantDetailViewController.swift
//  Chapter13_IntroductionOOP
//
//  Created by Vo The Dong An on 10/16/18.
//

import UIKit

class RestaurantDetailViewController: UIViewController {
    
    @IBOutlet private weak var restaurantDetailBackground: UIImageView!
    
    @IBOutlet private weak var restaurantNameLabel: UILabel!
    
    @IBOutlet private weak var restaurantTypeLabel: UILabel!
    
    @IBOutlet private weak var restaurantLocationLabel: UILabel!
    
    var restaurant: Restaurant?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showInfo()
    }
    
    private func showInfo() {
        guard let restaurant = restaurant else { return }
        restaurantNameLabel.text = restaurant.name
        restaurantTypeLabel.text = restaurant.type
        restaurantLocationLabel.text = restaurant.location
        restaurantDetailBackground.image = UIImage(named: restaurant.image)
    }
}
